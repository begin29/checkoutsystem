require "./product"
require "./fixtures/pricing_rules"
require "./checkout"

co = Checkout.new(PRICING_RULES)

# test 1
%w(APL1 COCA1 APL1 CHOC1 APL1 ).each do |item|
  co.scan(item)
end

p co.total
co.clean_products

# test 2
%w(CHOC1 BRE1 CHOC1 REB1 REB1 CHOC1).each do |item|
  co.scan(item)
end
p co.total
co.clean_products

# test 3
%w(REB1 COCA1 APL1 REB1 CHOC1 COCA1 REB1 APL1 APL1 BRE1).each do |item|
  co.scan(item)
end

p co.total
co.clean_products
