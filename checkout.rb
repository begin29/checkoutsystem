class Checkout
  attr_accessor :rules, :products

  def initialize(rules)
    @products = []
    @rules = rules
  end

  def scan(item_code)
    products << Product.new(item_code, rules[item_code])
    calculate_price_for_item(products.last)
  end

  def total
    products.inject(0) { |sum, prod| sum + prod.price }.round(2)
  end

  def clean_products
    @products = []
  end

  private
  def calculate_price_for_item(product)
    unless product.rule.nil?
      all_products_with_code = products_with_code(product.code, product.rule.fetch(:additinal_check, false))
      if all_products_with_code.count > product.rule[:amount]
        unchanged_products(all_products_with_code).each do |prod|
          change_product_price(prod, index_of_product(prod, all_products_with_code) )
        end
      end
    end
  end

  def change_product_price(product, index)
    product.price += apply_formula_for_price(product, index)
    product.changed = true
  end

  def index_of_product(product, all_products_with_code)
    all_products_with_code.find_index(product) + 1
  end

  def apply_formula_for_price(product, index)
    str =
      case product.rule[:frequency]
      when 'more'
        product.transformed_formula
      when 'each'
        product.transformed_formula if price_should_be_changed_for_product?(product, index)
      end
    return 0 unless str
    eval(str)
  end

  def price_should_be_changed_for_product?(product, index)
    (index != 0) && ( index % (product.rule[:amount]+1) == 0)
  end

  def products_with_code(item_code, additinal_check)
    if additinal_check
      products.select{|p| ( p.code == item_code || p.code == additinal_check )}
    else
      products.select{|p| ( p.code == item_code )} 
    end
  end

  def unchanged_products(all_products_with_code)
    all_products_with_code.select{|p| p.changed == false}
  end
end
