class Product
  PRODUCT_ITEMS = {
    "COCA1" => { name: 'Coca Cola', price: '0.89'},
    "REB1" => { name: 'Red Bull', price: '2.2'},
    "APL1" => { name: 'Apple', price: '3.45'},
    "CHOC1" => { name: 'Chocolate', price: '5.25'},
    "BRE1" => { name: 'Bread', price: '1.19'}
  }

  attr_accessor :code, :price, :changed, :rule

  def initialize(code, rule)
    @code = code
    @rule = rule
    @price = PRODUCT_ITEMS[code][:price].to_f
    @changed = false
  end

  def transformed_formula
    return '' unless rule
    rule[:price_formula].gsub(/(PRICE)/, 'PRICE' => price)
  end
end
