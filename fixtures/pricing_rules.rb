PRICING_RULES = {
  'APL1' => { amount: 2, price_formula: '-PRICE', frequency: 'each'}, 
  'CHOC1' => { amount: 2, price_formula: '4.7-PRICE', frequency: 'more'},
  'COCA1' => { amount: 4, price_formula: '-PRICE*0.1', additinal_check: 'REB1', frequency: 'more' },
  'REB1' => { amount: 4, price_formula: '-PRICE*0.1', additinal_check: 'COCA1', frequency: 'more' }
}
